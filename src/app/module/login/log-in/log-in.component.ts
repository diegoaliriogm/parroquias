import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  validateForm!: FormGroup;
  email: AbstractControl;
  password: AbstractControl;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }

  constructor(private fb: FormBuilder,private _auth: AngularFireAuth,private _authService:AuthService) {}

  ngOnInit(): void {

    this.validateForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }
  login(){

    this._authService.login({
      email: this.validateForm.controls.email.value,
      password: this.validateForm.controls.password.value,
      returnSecureToken: true
    }).subscribe((res) => console.log(res));

  }

}
