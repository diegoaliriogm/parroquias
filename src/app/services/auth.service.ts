import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { enviroment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url= enviroment.autUrlBase;
  key=enviroment.apiKey
  constructor(private _auth: AngularFireAuth,private _http: HttpClient) { 


  }

  public login(body: any): Observable<any> {
    return this._http.post(`${this.url}/v1/accounts:signInWithPassword?key=${this.key}`, body).pipe(
      map((res: any) => {
     
        return res;
      })
    );
  }
}
